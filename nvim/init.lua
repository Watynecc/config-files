vim.cmd [[packadd packer.nvim]]
require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'netsgnut/arctheme.vim'
  use 'junegunn/fzf'
  use 'junegunn/fzf.vim'
  use 'xiyaowong/transparent.nvim'
  use {
    'w0rp/ale',
    cmd = 'ALEEnable',
    config = 'vim.cmd([[ALEEnable]])'
  }
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  use {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",
  }

  use {
    'lewis6991/gitsigns.nvim',
    config = function()
    require('gitsigns').setup()
    end
  } 
  use 'ms-jpq/coq_nvim'
  use 'cocopon/iceberg.vim'
  -- Packer:
  use 'Mofiqul/vscode.nvim'
  use 'tanvirtin/monokai.nvim'
  use 'xiyaowong/nvim-transparent'
end)
require('nvim-treesitter.configs').setup {
    ensure_installed = "all",
    highlight = { enable = true },
    indent = { enable = true }
}
-- set
vim.opt.relativenumber = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.cmd([[colorscheme vscode]])
vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true
vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50


-- remap
vim.g.mapleader = " "
vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])
vim.keymap.set({"n", "v"}, "<leader>pa", [["+p]])
vim.keymap.set("n", "<leader>ps", [[:Files /home/watynecc/Documents/Projects]]) 
vim.keymap.set("n", "<leader>np", [[:Ntree /home/watynecc/Documents/Projects]]) 

-- lsp
vim.g.coq_settings = {
    auto_start = 'shut-up',
    clients = {
        tmux = { enabled = false },
    },
}

require("mason-lspconfig").setup_handlers({
     function (server_name) -- default handler (optional)
        require("lspconfig")[server_name].setup { require('coq').lsp_ensure_capabilities({})}
     end
})

-- Plugins

require('gitsigns').setup()



